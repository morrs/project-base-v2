module.exports = function(grunt){

	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        htmlhint: {

		    build: {

		        options: {
		            'tag-pair': true,
		            'tagname-lowercase': true,
		            'attr-lowercase': true,
		            'attr-value-double-quotes': true,
		            'doctype-first': true,
		            'spec-char-escape': true,
		            'id-unique': true,
		            'style-disabled': true
		        },
		        src: ['dev/*.html']

		    }

		},
		htmlmin: { 

			dist: {

				options: {                                 
					removeComments: true,
					collapseWhitespace: true,
				},
				files: [{
        			expand: true,                  
        			cwd: './dist/',                   
        			src: ['*.html'],
        			dest: 'dist/' 
      			}]
			}   

		},
		sass: {  

		    dev: {

		    	options: {                       
					style: 'expanded',
					sourcemap: true,
					quiet: true,
					debugInfo: true
				},
				files: {
					'dev/assets/css/styles.css': 'dev/assets/sass/main.scss'
				}

		    },
		    dist: {

		    	options: {                       
					style: 'compressed'
				},
				files: {
					'dist/assets/css/styles.css': 'dev/assets/sass/main.scss'
				}

		    }

  		},
		autoprefixer: {

		    dev: {		 
				src: 'dev/assets/css/styles.css',
				dest: 'dev/assets/css/styles.css'
		    },
		    dist: {
		    	src: 'dist/assets/css/styles.css',
				dest: 'dist/assets/css/styles.css'
		    }

	  	},
		concat: {

	        files: {
	            src : ['dev/assets/bower_components/jquery/dist/jquery.min.js', 'dev/assets/js/init.js'],
	            dest : 'dev/assets/js/scripts.js'
	        }

	    },
	    uglify: {

	    	my_target: {
		    
		        files: {
		            'dist/assets/js/scripts.js': ['dev/assets/js/scripts.js'],
		            'dist/assets/js/modernizr.js': ['dev/assets/bower_components/modernizr/modernizr.js']
		        }

		    }
		    
		},
		sprite: {
            
            create: {
		        src: 'dev/assets/img/sprite_assets/*.png',
		        destImg: 'dev/assets/img/sprite.png',
		        imgPath: '../img/sprite.png',
		        destCSS: 'dev/assets/sass/module/_sprites.scss',
		        // OPTIONAL: Specify algorithm (top-down, left-right, diagonal [\ format],
          		// alt-diagonal [/ format], binary-tree [best packing])
      			// Visual representations can be found below
      			algorithm: 'binary-tree',
      			// OPTIONAL: Specify padding between images
      			padding: 750,
      			// OPTIONAL: Specify img options
				imgOpts: {
					format: 'png',
				 	quality: 100
				}
	      	}
            
        },
		imagemin: {

			png: {
				options: {
					optimizationLevel: 7
				},
				files: [
					{
						expand: true,
						cwd: 'dev/assets/img/',
						src: ['*.png'],
						dest: 'dist/assets/img/',
						ext: '.png'
					}
				]	
			},
			jpg: {
				options: {
					progressive: true
				},
				files: [
					{
						expand: true,
						cwd: 'dev/assets/img/',
						src: ['*.jpg'],
						dest: 'dist/assets/img/',
						ext: '.jpg'
					}
				]
			}

		},
		hashres: {

			options: {
				encoding: 'utf8',
				fileNameFormat: '${name}-${hash}.${ext}',
				renameFiles: true
			},
			prod: {			
				src: [
				// WARNING: These files will be renamed!
				'dist/assets/js/scripts.js',
				'dist/assets/css/styles.css'],
				// File that refers to above files and needs to be updated with the hashed name
				dest: 'dist/*.html',
			}

		},
		includes: {

			files_dev: {
				src: ['dev/staging/*.html'], // Source files
				dest: 'dev/', // Destination directory
				cwd: '.',
				flatten: true
			},
			files_dist: {
				src: ['dev/staging/*.html'], // Source files
				dest: 'dist/', // Destination directory
				cwd: '.',
				flatten: true
			}

		},
		watch: {

			html: {
                files: ['dev/staging/**/*.html'],
                tasks: ['includes:files_dev', 'htmlhint', 'htmlmin', 'growl:watch']
            },
			scss: {
                files: ['dev/assets/sass/**/*.scss'],
                tasks: ['sass:dev', 'autoprefixer', 'growl:watch']
            },
            js: {
                files: ['dev/assets/js/*.js'],
                tasks: ['concat', 'growl:watch']
            },
            options: {	        
		        livereload: true
	        }

        },
        concurrent: {

        	dev: ['htmlhint', 'sass:dev', 'concat'],
		    dist: ['sass:dist', 'uglify']

		},
		growl: {

			watch: {
				message :"Development file/s built",
				title: "Watch success"
			},
			build: {
				message: 'Build complete',
				title: 'Site build'
			},
			sprite: {
				message: 'Sprite successfully created',
				title: 'Sprite success'
			}

		}

    });

	grunt.registerTask('default', [
		'includes:files_dev',
		'concurrent:dev',
		'autoprefixer:dev',
		'growl:build'
    ]);

    grunt.registerTask('build', [
    	'includes:files_dist',
		'concurrent:dist',
		'autoprefixer:dist',
		'hashres',
		'htmlmin:dist',
		'imagemin',
		'growl:build'
    ]);

    grunt.registerTask('build-sprite', ['sprite', 'growl:sprite']);

};